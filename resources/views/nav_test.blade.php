<!-- Navbar-->
<nav class="navbar navbar-expand-lg navbar-white bg-white main-navigation fixed-top border border-bottom">
  <div class="container-fluid justify-content-between">
    <!-- Left elements -->
    <div class="d-flex">
      <!-- Brand -->
      <a class="navbar-brand d-flex align-items-center" href="/home">
        <img
          src="/media/logo/NFP.png"
          height=30
          alt=""
          loading="lazy"
          style="margin-top: 1px; object-fit: cover"
        />
      </a>

      <!-- Search form -->
      <form class="input-group w-auto my-auto d-sm-flex">
        <input
          class="form-control rounded"
          placeholder="Search"
        />
        <span class="input-group-text border-0 d-none d-lg-flex"
          ><i class="fa fa-search"></i
        ></span>
      </form>
	        

    </div>
	

    <!-- Left elements -->
	<ul class="d-sm-flex d-md-none list-group float-right">
<li class="nav-item dropdown  me-lg-1 ">
	  <button class="btn dropdown-toggle mr-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" >
          <i class="text-success fa fa-bars fa-lg"></i>
      </button>
	  <div class="dropdown-menu dropdown-menu" aria-labelledby="dropdownMenuButton" style="right: 0; left: auto;">
    <a class="dropdown-item" href="#"><i class="fa fa-bell fa-xs"></i> Notifications <span class="badge rounded-pill badge-notification bg-danger text-white ">6</span></a>
    <a class="dropdown-item" href="#"><i class="fa fa-comments fa-xs"></i> Messages <span class="badge rounded-pill badge-notification bg-danger text-white">6</span></a>
    <a class="dropdown-item" href="#"><i class="fa fa-cog fa-xs"></i> Settings</a>
    <a class="dropdown-item" href="#"><i class="fa fa-sign-out fa-xs"></i> Logout</a>
  </div>
	  </li>
	  </ul>
    <!-- Right elements -->
    <ul class="navbar-nav flex-row d-none d-lg-flex">
      <li class="nav-item dropdown me-3 me-lg-1">
	  <button class="btn dropdown-toggle mr-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" >
          <i class="fa fa-bell fa-sm"></i>
          <span class="badge rounded-pill badge-notification bg-danger text-white">7</span>
      </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="right: 0; left: auto;">
    <a class="dropdown-item" href="#">Ashik Rawal commented on your post. <small class="text-muted">2 hr ago.</small></a><hr>
    <a class="dropdown-item" href="#">Ahsan Elahi liked your post. <small class="text-muted">2 hr ago.</small></a><hr>
    <a class="dropdown-item" href="#">Dipa Giri commented on your post. <small class="text-muted">2 hr ago.</small></a><hr>
    <a class="dropdown-item" href="#">Bishesh Thapa liked your post. <small class="text-muted">2 hr ago.</small></a><hr>
    <a class="dropdown-item" href="#">Samita Magar commented on your post. <small class="text-muted">2 hr ago.</small></a><hr>
    <a class="dropdown-item" href="#">Arjun Lama liked your post. <small class="text-muted">2 hr ago.</small></a><hr>
    <a class="dropdown-item" href="#">Dipu Gotame commented on your post. <small class="text-muted">2 hr ago.</small></a><hr>
  </div>
	  </li>
	  
      <li class="nav-item dropdown me-3 me-lg-1">
	  <button class="btn dropdown-toggle mr-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-comments fa-sm"></i>
          <span class="badge rounded-pill badge-notification bg-danger text-white">1</span>
      </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="right: 0; left: auto;">
    <a class="dropdown-item" href="#"><span class="bg-success text-white p-1">John Smith</span>  You: Hi There!<small class="text-muted"> 2 hr ago.</small></a><hr>
    <a class="dropdown-item" href="#"><span class="bg-success text-white p-1">Dipa Giri</span>  Hi There!<small class="text-muted"> 2 hr ago.</small></a><hr>
  </div>
	  </li>
	  
	        <li class="nav-item dropdown">
        <a class="nav-link d-sm-flex align-items-sm-center dropdown-toggle" href="#"  role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre id="navbarDropdown" >
          <img
            src="{{Auth::user()->profile_pic()}}"
            class="rounded-circle"
            height="22px"
            width="22px"
            alt=""
            style="object-fit: cover"
          />
          <strong class="d-none d-sm-block ms-1">{{ Auth::user()->name }}</strong>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('edit_profile', ['user_id' => Auth::user()->id])  }}"
                                      >
                                        <i class="fa fa-user text-success mr-2"></i>{{ __('Edit Profile') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                      >
                                        <i class="fa fa-cog text-success mr-2"></i>{{ __('Settings') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                      >
                                        <i class="fa fa-question-circle text-success mr-2"></i>{{ __('Help') }}
                                    </a>
                                    <hr>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out text-success mr-2"></i>{{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>

      </li>

    </ul>
    <!-- Right elements -->
  </div>
</nav>
<!-- Navbar -->
