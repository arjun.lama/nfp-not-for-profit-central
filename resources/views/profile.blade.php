@extends('master')
@section('main-body')
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
.profile-cover{
height: 300px;
}
img.profile-image {
    max-width: 100%;
    max-height: 100%;
	object-fit: cover;
	height: 220px;
}
.profile-image-div {
width: 23%; 
height:300px;
}
@media only screen and (max-width: 600px) {
  .profile-image-div {
    width: 100%;
	height: 250px;
	margin: auto;
  }
}


.profile-desc-div{
background-color: rgba(255,255,255,1);
width: 73%; 
height:285px;
margin-top: 10px;
}

.profile-thumb-image{
width: 100px;
height: 100px;
object-fit: cover;
}
</style>
</head>
<div class="container p-2 card " style="margin-top:100px; background-color: rgba(0,0,0,0.3)">
@include('profile_cover')
<hr class="bg-white mt-5">
<div class="row">
  <div class="col-sm-8  p-2">
    @foreach ($user_posts as $post)

      <div class="card text-left p-2 mb-2 shadow-lg">
        <div class="name-and-pic">
<img class="rounded-circle d-inline" src="{{$post -> user ->profile_pic()}}" width="50px" height="50px" style="object-fit: cover">
<h6 class="ml-2">{{ $post -> user ->name }} <br><small class="text-muted">{{$post->user->organization}}</small></h6>
</div>
<hr>
<h6 class="text-muted">{{ $post->post_text }}</h6><br>
@if ($post->post_type == 'PHOTOUPLOAD')
@foreach ($post->media as $medium)
<img src="media/{{ $medium->media_name}}" height="400px" style="object-fit: cover">
@endforeach
@endif

@if ($post->post_type == 'VIDEOUPLOAD')
@foreach ($post->media as $medium)
<video width="400" height="350" controls>
<source src="media/{{ $medium->media_name}}" style="object-fit: cover">
</video>
@endforeach
@endif

@if ($post->post_type == 'EVENT')
<div class="card" style="width: 400px;">
  <div class="card-body">

    <h5 class="card-title text-primary">{{$post->events['event_name']}}</h5>
    <hr>
    <h6 class="card-subtitle mb-2 text-muted"><i class="fa fa-location-arrow mr-2 text-success"></i>{{$post->events['event_venue']}} </h6>
    <h6 class="card-text"><i class="fa fa-clock-o mr-2 text-success"></i>{{$post->events['event_date']}} <em class="text-muted">@  {{$post->events['event_time']}}</em></h6>
    <h6 class="card-text"><i class="fa fa-users mr-2 text-success"></i><em class="text-muted">{{$post->events['event_type']}}</em></h6>
    <hr>
    <a href="#" class="btn btn-outline-success">Attending</a>
    <a href="#" class="btn btn-outline-danger">Not Attending</a>
  </div>
</div>
@endif

<br>
<small class="">{{ $post->created_at->diffForHumans() }}</small>
<hr>
<div class="row p-2">
  <div class="col-xs-6 mx-3 mx-sm-3 mx-md-3 mx-lg-5">
<a><i class="fa fa-heart text-danger" style="display:inline-block"> &nbsp;Like</i></a></div>
  <div class="col-xs-6 mx-3 mx-sm-3 mx-md-3 mx-lg-5">
<a><i class="fa fa-comment text-primary"  style="display:inline-block"> &nbsp;Comment</i></a></div>
</div>
      </div>
@endforeach

  </div>
  <div class="col-sm-4 d-none d-lg-block p-2">
  
  @include('chat')
  
  </div>
</div>
</div>
<script>
$(document).ready(function(){
    $(".profile-component").hide();
    $("#profile-about").show();

  $("#button-about").click(function(){
    $(".profile-component").hide();
    $("#profile-about").show();
  });
  $("#button-media").click(function(){
    $(".profile-component").hide();
    $("#profile-media-photos").show();
    $("#profile-media-videos").show();
  });
  $("#button-events").click(function(){
    $(".profile-component").hide();
    $("#profile-events").show();
  });
});
</script>
