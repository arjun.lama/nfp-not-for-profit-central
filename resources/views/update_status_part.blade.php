<div class="card bg-white text-center mb-2 col-xs-12 col-lg-9 align-center" style="margin:auto">
  <div class="card-header bg-white">
    <ul class="nav nav-pills card-header-pills">
      <li class="nav-item">
        <a class="btn btn-sm btn-outline-primary mr-2" data-toggle="modal" data-target="#modalPhoto" href="#">Photo</a>
      </li>
      <li class="nav-item">
        <a class="btn  btn-sm btn-outline-primary mr-2" data-toggle="modal" data-target="#modalVideo" href="#">Video</a>
      </li>
      <li class="nav-item">
        <a class="btn  btn-sm btn-outline-primary mr-2" data-toggle="modal" data-target="#modalEvent" href="#">Event</a>
      </li>
    </ul>

<!--photo modal -->	
	<div class="modal fade" id="modalPhoto" tabindex="-1" role="dialog" aria-labelledby="modalPhotoCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-success" id="modalPhotoLongTitle">Upload Photos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form method="post" action="/upload_photo" id="photo_update_form" enctype="multipart/form-data">
    @csrf
      </form>

<div class="form-group text-left text-success">
<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text "><i class="fa fa-image text-primary ml-1"></i></span>
  </div>
    <input type="file" class="bg-light" id="inputGroupFile01" form="photo_update_form" name="upload_photo" title="Choose Photos">
</div>
    

    Photo Caption
    <textarea class="form-control form-control-sm mb-1" id="photoCaption" rows="3" form = "photo_update_form" name="photo_caption"></textarea>
		Privacy<select class="form-control" form = "photo_update_form" name="photo_privacy">
      <option value="private">Connections Only</option>
      <option value="public">Everyone</option>
	</select>

  </div>      
</div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" style="width: 100%" form = "photo_update_form" value="Upload">
      </div>
    </div>
  </div>
</div>

<!--photo modal end -->	

<!--video modal -->	
	<div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="modalPhotoCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-success" id="modalPhotoLongTitle">Upload Videos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                    <form method="post" action="/upload_video" id="video_update_form" enctype="multipart/form-data">
    @csrf
      </form>

<div class="form-group text-left text-success">
<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text "><i class="fa fa-video-camera"></i></span>
  </div>
    <input type="file" class="bg-light" id="inputGroupFile01" form="video_update_form" name="upload_video" title="Choose Videos">
</div>
    

    Video Description
    <textarea class="form-control form-control-sm mb-1" id="videoCaption" rows="3" name="video_caption" form="video_update_form"></textarea>
		Privacy<select class="form-control" form="video_update_form" name="video_privacy">
      <option value="private">Connections Only</option>
      <option value="public">Everyone</option>
	</select>

  </div>      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" style="width: 100%" form="video_update_form">
      </div>
    </div>
  </div>
</div>

<!--video modal end -->	

<!--event modal -->	
	<div class="modal fade" id="modalEvent" tabindex="-1" role="dialog" aria-labelledby="modalPhotoCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-success" id="modalPhotoLongTitle">Create Events</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    <form method="post" action="/create_event" id="event_create_form" enctype="multipart/form-data">
    @csrf
      </form>

<div class="form-group text-left text-success container">
Event Name
<div class="input-group mb-1">
  <div class="input-group-prepend">
    <span class="input-group-text "><i class="fa fa-calendar"></i></span>
  </div>
  
    <input type="text" class="form-control" id="eventName" form="event_create_form" name="event_name">
</div>
    
Event Date and Time
<div class="row mb-1">
<div class="input-group col-sm-6">
  <div class="input-group-prepend">
    <span class="input-group-text "><i class="fa fa-calendar"></i></span>
  </div>
  
    <input type="date" class="form-control" id="eventName" form="event_create_form" name="event_date">
</div>
<div class="input-group col-sm-4">
  <div class="input-group-prepend">
    <span class="input-group-text "><i class="fa fa-clock-o"></i></span>
  </div>
  
    <input type="time" class="form-control" id="eventName" form="event_create_form" name="event_time">
</div>

</div>
    Event Description
    <textarea class="form-control form-control-sm mb-1" id="photoCaption" rows="3" form="event_create_form" name="event_desc"></textarea>
    Event Location
    <textarea class="form-control form-control-sm mb-1" id="photoCaption" rows="1" form="event_create_form" name="event_location"></textarea>
		Event Type<select class="form-control mb-1" form="event_create_form" name="event_type"> 
      <option value="virtual">Virtual</option>
      <option value="physical">Physical</option>
	</select>
      Participants Type<select class="form-control" form="event_create_form" name="part_type">
      <option value="local">Local</option>
      <option value="global">Global</option>
  </select>

		Privacy<select class="form-control" form="event_create_form" name="event_privacy">
      <option value="private">Connections Only</option>
      <option value="public">Everyone</option>
	</select>

  </div>      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-primary" style="width: 100%" form="event_create_form">
      </div>
    </div>
  </div>
</div>

<!--event modal end fd-->	

  </div>
  <div class="card-body">
<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text d-sm-flex" id="inputGroup-sizing-sm"><img class="rounded-circle" src="{{Auth::user()->profile_pic()}}" style="width: 50px;height:50px; object-fit: cover"></span>
  </div>
  <form method="post" action="/submit_status" id="status_update_form">
    @csrf
      </form>

  <input form="status_update_form" type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" placeholder="Share Something!" name="status">
  <input form="status_update_form" type="submit" class="btn btn-primary mt-1" style="width: 100%" value="Share">
    </div>
  </div>
</div>
