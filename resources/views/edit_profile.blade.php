

@extends('master')
@section('main-body')

<div class="profile-cover row" style="margin-top: 100px">
<div class="col-sm-4 text-center" style="">
<img class="rounded profile-image img-responsive img-thumbnail" src="{{$user_details->profile_pic()}}" style="width:250px; height:250px;"><br>
<div class="bottom-right btn">Change</div>
</div>
<div class=" card col-sm-8" style="width" >
<br><br>
<div class="profile-component profile-about" id="profile-about">
<h5>{{$user_details->name}}</h5><hr>
Organisation: {{$user_details->organization}}<hr>
Location: <hr>
Interests: Travelling, Movies, Web Dev<hr>
</div>

</div>
</div>
