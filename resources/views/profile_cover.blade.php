
<div class="profile-cover" style="margin-top: 10px; margin-left: 30px;">
<div class="d-inline-block  float-left text-center profile-image-div mt-4" style="">
<img class="rounded profile-image img-responsive img-thumbnail" src="{{$user_details->profile_pic()}}" style="width:250px; height:250px;"><br>
@if ($follow_status==false)
<a class="btn btn-primary mt-2 text-white" href="{{ route('follow_user', ['user_id' => $user_details ->id])  }}">Follow<strong> +</strong></a>
@elseif ($follow_status==true)
<a class="btn btn-primary mt-2 text-white" href="{{ route('unfollow_user', ['user_id' => $user_details ->id])  }}">Unfollow<strong> </strong></a>
@endif
</div>
<div class="d-none d-lg-inline-block p-2 float-left pr-5 card profile-desc-div ml-4" style="width:700px;">
<button class="btn btn-outline-primary" id="button-about">About</button>
<button class="btn btn-outline-primary" id="button-media">Media</button>
<button class="btn btn-outline-primary" id="button-events">Events</button>
<br><br>
<div class="profile-component profile-about" id="profile-about">
<h5>{{$user_details->name}}</h5><hr>
Organisation: {{$user_details->organization}}<hr>
Location: <hr>
Interests: Travelling, Movies, Web Dev<hr>
</div>
<div class="profile-component profile-media p-2" id="profile-media-photos">
	@foreach ($media_details as $media)
	@if ($media->media_type=='PHOTO')
 <a href=""><img class="d-inline-block img-thumbnail profile-thumb-image float-left mr-2" src="{{$media->media_url}}" style="width:75px; height:75px; object-fit: cover"></a>
	@elseif ($media->media_type=='VIDEO')
	<video width="75px" height="75px" controls>
<source src="{{ $media->media_url}}" style="object-fit: cover">
</video>
@endif
	@endforeach

<div class="profile-component profile-events" id="profile-events">
</div>
</div>
</div>
</div>
