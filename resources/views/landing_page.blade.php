<html>
<head>
<title>NFP Central</title>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.bgimg{

background-image:url('https://cdn.pixabay.com/photo/2018/04/18/18/47/hands-3331216_960_720.jpg');
background-repeat:no-repeat;
background-size: cover;
background-position: center center;
width: 100%;
height: 100%;
background-attachment: fixed;
}

.bgimg2{
background-image:url('https://outcomes.business/wp-content/uploads/2013/09/shake-up-sales-meeting-og.jpg');
width: 40%;
background-size:cover;
height: 200px;
}

.bgimg3{
background-image:url('https://enterprisersproject.com/sites/default/files/cio_open_source_meeting_tools.png');
width: 40%;
background-size:cover;
height: 200px;
}

.bgimg4{
background-image:url('https://sloanreview.mit.edu/wp-content/uploads/2020/05/GEN-Rogelberg-Remote-Virtual-Zoom-Meeting-Video-Conference-2400x1260-1-1200x630.jpg');
width: 40%;
background-size:cover;
height: 200px;
}

.main-container{
}

.top-container{
width: 100%;
height: 315px;
background-color: rgba(0,0,0,0.5);
}

.left-container{
background-color: rgba(255,255,255,0.5);
margin-top: 20px;
padding: 10px;
}
.right-container{
}
.main-heading{
}

.button-container{
position:fixed;
bottom: 20px;
right: 20px;
background-color: rgba(0,0,0,0.1);
padding: 15px;
border-radius: 5px;
}

.button-container-small{
position:fixed;
top: 10px;
right: 10px;
padding: 10px;
border-radius: 5px;
}

</style>
<body>
<div class="bgimg main-container">
<div class="top-container">
<div class="w3-content w3-display-container ">
<div class="button-container-small w3-hide-large">
<a href="login.php" class="w3-button  w3-text-white w3-border w3-border-white w3-round-large">Log In</a>
<a href="signup.php" class="w3-button  w3-text-white w3-border w3-border-white w3-round-large">Sign Up</a>
</div>
    
	<div class="mySlides w3-row w3-padding-64 w3-center" style="width:100%">
  <div class="w3-half"><h1 class="main-heading w3-text-white">One Platform Multiple Connections</h1><br><span class="w3-text-white">Know everything about us<br> through this short introductory video</span><br><i class="fa fa-play-circle w3-text-white w3-xlarge w3-hide-large w3-border w3-border-green w3-padding">&nbsp; Play</i></div>
 <a href=""> <div class="w3-half w3-hide-small bgimg2 main-heading w3-display-container w3-padding w3-hover-opacity"><i class="fa fa-play-circle w3-display-middle w3-jumbo w3-text-black"></i></div> </a>
  </div>

<div class="mySlides w3-row w3-padding-64 w3-center" style="width:100%">
  <div class="w3-half"><h1 class="main-heading w3-text-white">Our Services</h1><br><span class="w3-text-white">Watch the video<br> to know more about the system features.</span><br><i class="fa fa-play-circle w3-text-white w3-xlarge w3-hide-large w3-border w3-border-green w3-padding">&nbsp; Play</i></div>
 <a href=""> <div class="w3-half w3-hide-small bgimg3 main-heading w3-display-container w3-padding w3-hover-opacity"><i class="fa fa-play-circle w3-display-middle w3-jumbo w3-text-black"></i></div> </a>
  </div>


  <div class="mySlides w3-row w3-padding-64 w3-center" style="width:100%">
  <div class="w3-half"><h1 class="main-heading w3-text-white">Connect with your fellows</h1><br><span class="w3-text-white">Learn<br> how to connect and share</span><br><i class="fa fa-play-circle w3-text-white w3-xlarge w3-hide-large w3-border w3-border-green w3-padding">&nbsp; Play</i></div>
 <a href=""> <div class="w3-half w3-hide-small bgimg4 main-heading w3-display-container w3-padding w3-hover-opacity"><i class="fa fa-play-circle w3-display-middle w3-jumbo w3-text-black"></i></div> </a>
  </div>

  <button class="w3-button w3-black w3-display-left w3-margin-right" onclick="plusDivs(-1)">&#10094;</button>
  <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
</div>
</div>
<div class="bottom-container w3-row">
<div class="w3-half left-container">
<i class="fa fa-user-circle w3-jumbo w3-text-green"></i><span class="w3-xlarge margin-bottom w3-text-green">CEO</span><br><span class="w3-text-green">Mr.Scott Lockie</span><br><br>
"This platform is designed with the sole purpose of establishing connection with the people around the world.
Our aim is to connect people working with charities and non-profit organizations. When people can share ideas in a single platform,
productivity increases. This has been already proved by giant social media sites like LinkedIn and Facebook.
The main objective of our platform is to allow users share information, innovations and media."</div>
<div class="w3-half right-container w3-hide-small">
<div class="button-container">
<a href="/login" class="w3-button w3-white w3-text-green w3-border w3-border-green w3-round-large">Log In</a>
<a href="/register" class="w3-button w3-white w3-text-green w3-border w3-border-green w3-round-large">Sign Up</a>
</div>
</div>
</div>
</div>
</body>
<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>
