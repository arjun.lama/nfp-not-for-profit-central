@extends('master')

@section('main-body')

<style>
  .name-and-pic{
    display: flex;
  align-items: center;
  flex-direction: row;
  }
</style>
<div class="container p-2 card " style="margin-top:75px; background-color: rgba(0,0,0,0.3)">
@include('update_status_part')
<div class="row">
  <div class="col-sm-8  p-2">
    @foreach ($posts as $post)

			<div class="card text-left p-2 mb-2 shadow-lg">
<a href="{{ route('profile_link', ['user_id' => $post -> user ->id])  }}">       
 <div class="name-and-pic">
<img class="rounded-circle d-inline" src="{{$post -> user ->profile_pic()}}" width="50px" height="50px" style="object-fit: cover">
<h6 class="d-inline ml-2">{{ $post -> user ->name }} <br><small class="text-muted">{{$post->user->organization}}</small></h6>
</div></a>
<hr>
<h6 class="text-muted">{{ $post->post_text }}</h6><br>
@if ($post->post_type == 'PHOTOUPLOAD')
@foreach ($post->media as $medium)
<img src="media/{{ $medium->media_name}}" height="400px" style="object-fit: cover">
@endforeach
@endif

@if ($post->post_type == 'VIDEOUPLOAD')
@foreach ($post->media as $medium)
<video width="400" height="350" controls>
<source src="media/{{ $medium->media_name}}" style="object-fit: cover">
</video>
@endforeach
@endif

@if ($post->post_type == 'EVENT')
<div class="card" style="width: 400px;">
  <div class="card-body">

    <h5 class="card-title text-primary">{{$post->events['event_name']}}</h5>
    <hr>
    <h6 class="card-subtitle mb-2 text-muted"><i class="fa fa-location-arrow mr-2 text-success"></i>{{$post->events['event_venue']}} </h6>
    <h6 class="card-text"><i class="fa fa-clock-o mr-2 text-success"></i>{{$post->events['event_date']}} <em class="text-muted">@  {{$post->events['event_time']}}</em></h6>
    <h6 class="card-text"><i class="fa fa-users mr-2 text-success"></i><em class="text-muted">{{$post->events['event_type']}}</em></h6>
    <hr>
    <a href="#" class="btn btn-outline-success">Attending</a>
    <a href="#" class="btn btn-outline-danger">Not Attending</a>
  </div>
</div>
@endif

<br>
<small class="">{{ $post->created_at->diffForHumans() }}</small>
<hr>
<div class="row">
  <div class="col-xs-6 mx-3 mx-sm-3 mx-md-3 mx-lg-3">
<button class="like-button btn btn-dark" post-id = "{{ $post->post_id }}"><i class="fa fa-heart" style="display:inline-block"> Like</i></button>
</div>
  <div class="col-xs-6 mx-3 mx-sm-3 mx-md-3 mx-lg-5">
<button class="comment-button btn btn-dark" post-id = "{{ $post->post_id }}"><i class="fa fa-comment" style="display:inline-block"> Comment</i></button>
</div>
</div>
<div class="comment-section" display="hidden" post-id="{{ $post->post_id }}">
<hr>  <small class="muted-text">Put your comments here</small>

<form class="comment-form" action="/add_comment" method="post"  post-id="{{ $post->post_id }}">
  @csrf
  <input type="number" name="post_id" hidden value="{{$post->post_id}}">
  <textarea class="input-group m-1 comment-textarea" name="comment_text" rows="2" cols="20"></textarea>
  <input type="submit" class="btn btn-primary comment-post-button" value="Comment">
  </form>
  <div post-id="{{$post->post_id}}" class="comment-each">

</div>
</div>
			</div>
@endforeach

  </div>
  <div class="col-sm-4 d-none d-lg-block p-2">
  
  @include('chat')
  
  </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
  $(document).ready(function(){
    $('.comment-section').hide();
  $('.comment-button').click(function(){
    showComments($(this).attr('post-id'));
  });

function showComments(postId){ 
    $('.comment-section').hide();
$.ajax({
    type:"GET",
    url:"/get_comments",
    data: {post_id:postId},
   success:function(data){
    var comment_section = $('.comment-each[post-id='+postId+']');
      comment_section.html('');
    $.each(data, function(i,val) { 
      comment_section.append('<div class="card bg-light d-block m-1 pt-2 pl-1"><div class="name-and-pic d-inline-block mr-2"><img class="rounded-circle d-inline" src="/media/profile_pic/'+val.user_p_pic+'" width="20px" height="20px" style="object-fit: cover"><p class="font-weight-bold d-inline-block">'+val.name+'</p></div>' + val.comment_text +'</div>');
     //comment_section.append(val.comment_text + '</div>')
    });   
    },
    error:function (request) {
        console.log(request.responseText);
    }


  });
$('.comment-section[post-id='+postId+']').show();

}

$(".comment-form").submit(function (e){
  e.preventDefault();
  form = $(this);
  url = form.attr('action');
  $.ajax({
    type:"POST",
    url:url,
    data: form.serialize(),
   success:function(data){
      //alert(form.attr('post-id'));
      $('.comment-textarea').val("");
      showComments(form.attr('post-id'));

    }


  });
});

});
</script>
