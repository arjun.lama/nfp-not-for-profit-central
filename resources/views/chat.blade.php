<div class="card text-left p-3 mb-2 shadow ">
					<h6>Say Hi! to Your Connections</h6>
  <hr>
   <a data-toggle="modal" data-target="#modalChat" href="#" href="" style="text-decoration:none"><div class="online-connections"><h5 class="text-success"><i class="fa fa-circle"></i> <small class="text-dark">Samita Magar</small></h5><hr></div></a>
   <a href="" style="text-decoration:none"><div class="online-connections"><h5 class="text-warning"><i class="fa fa-circle"></i> <small class="text-dark">Dipa Giri</small></h5><hr></div></a>
   <a href="" style="text-decoration:none"><div class="online-connections"><h5 class="text-danger"><i class="fa fa-circle"></i> <small class="text-dark">Dipu Gotame</small></h5><hr></div></a>
   <a href="" style="text-decoration:none"><div class="online-connections"><h5 class="text-danger"><i class="fa fa-circle"></i> <small class="text-dark">Scott Lockie</small></h5><hr></div></a>
   <a href="" style="text-decoration:none"><div class="online-connections"><h5 class="text-success"><i class="fa fa-circle"></i> <small class="text-dark">Arjun Lama</small></h5><hr></div></a>
   <a href="" style="text-decoration:none"><div class="online-connections"><h5 class="text-success"><i class="fa fa-circle"></i> <small class="text-dark">Bishesh Thapa</small></h5><hr></div></a>
   <a href="" style="text-decoration:none"><div class="online-connections"><h5 class="text-success"><i class="fa fa-circle"></i> <small class="text-dark">Ahsan Elahi</small></h5><hr></div></a>
   <a href="" style="text-decoration:none"><div class="online-connections"><h5 class="text-success"><i class="fa fa-circle"></i> <small class="text-dark">Ashik Rawal</small></h5><hr></div></a>
   <a href="" style="text-decoration:none"><div class="online-connections"><h5 class="text-success"><i class="fa fa-circle"></i> <small class="text-dark"> Lin Yue</small></h5><hr></div></a>
   <a href="" style="text-decoration:none"><div class="online-connections"><h5 class="text-success"><i class="fa fa-circle"></i> <small class="text-dark">Sweta Thakur</small></h5><hr></div></a>
			</div>
			
				<div class="modal fade" id="modalChat" tabindex="-1" role="dialog" aria-labelledby="modalPhotoCenterTitle" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-success" id="modalPhotoLongTitle">Samita Magar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <div class="photo-message">
<img src="https://www.pngitem.com/pimgs/m/22-223925_female-avatar-female-avatar-no-face-hd-png.png" height="50px" width="50px" style="object-fit:cover; border-radius: 75px" class=""><div class="p-1 align-middle m-2 d-inline align-middle rounded my-auto bg-success text-white rounded-lg">Hi How are you?</div>
</div>

	  <div class="photo-message float-right mb-3">
<div class="p-1 align-middle m-2 d-inline align-middle rounded my-auto bg-success text-white rounded-lg">I am good. How about you?</div><img src="https://www.pngitem.com/pimgs/m/22-223925_female-avatar-female-avatar-no-face-hd-png.png" height="50px" width="50px" style="object-fit:cover; border-radius: 75px" class="">
</div>
 <div class="type-send float-left mt-3 " style="">
<input class="p-2 mr-1 align-middle d-inline align-middle rounded my-auto border border-primary rounded-lg" placeholder="Type your message.."  style=""></input>
<button class="btn btn-primary py-2 float-right"><i class="fa fa-paper-plane "></i></button>
</div>

</div>
</div>
</div>
</div>