<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->string('m_name')->nullable()->after('name');
            $table->string('l_name')->nullable()->after('m_name');
            $table->string('phone')->nullable()->after('email');
            $table->string('organization')->nullable()->after('phone');
            $table->string('user_type')->nullable()->after('password');
            $table->string('user_status')->nullable()->after('user_type');
            $table->string('user_interests')->nullable()->after('user_status');
            $table->string('user_location')->nullable()->after('user_interests');
            $table->string('user_p_pic')->nullable()->after('user_location');
           
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
