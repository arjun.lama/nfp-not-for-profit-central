<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Post extends Model
{
    //
	protected $table = 'posts_table';
	function user(){
		return $this->belongsTo('App\User','owner_id','id');

	}

	function media(){
		return $this->hasMany('App\Media','post_id','post_id');
		
	}

	function events(){
		return $this->hasOne('App\Event','post_id','post_id');
		
	}

	function comments(){
		return $this->hasMany('App\Comment','post_id','post_id');
	}



	
}
