<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    protected $table='media_table';
    function post(){
    	return $this->belongsTo('App\Post','media_id','post_media_id');
    }
    function user(){
    	return $this->belongsTo('App\User','media_owner_id','id');
    }
}
