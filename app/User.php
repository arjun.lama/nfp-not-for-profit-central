<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'm_name','l_name','phone','organization','user_type','user_status','email', 'password','user_location','user_interests','user_p_pic',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    function posts(){
        return $this->hasMany('App\Post','owner_id');
    }

    function following(){
        return $this->belongsToMany('App\User','followers','user_id','following_id');
    }

    public function profile_pic()
    {
        if (! $this->user_p_pic) {
            return '/media/profile_pic/default_profilepic.png';
        }
        return '/media/profile_pic/'.$this->user_p_pic;
    }
}
