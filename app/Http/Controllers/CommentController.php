<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use App\Post;

class CommentController extends Controller
{
    //
    public function createComment(Request $request){
    	$comment = new Comment;
    	$comment->owner_id = Auth::user()->id;
    	$comment->post_id = $request->post_id;
    	$comment->comment_text = $request->comment_text;
    	$comment->comment_status='NEW';
    	$comment->save();
    	return redirect()->back();
    }

    public function getComments(Request $request){
    	$post_id = $request->post_id;
    	$comments = Comment::where('post_id',$post_id)
    				->join('users','users.id','=','comments_table.owner_id')
    				->orderBy('comments_table.id','DESC')
    				->get();
    	return response()->json($comments);

    }
}
