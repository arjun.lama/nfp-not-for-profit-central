<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Follower;
use Illuminate\Http\Request;

class FollowUser extends Controller
{
    //
    public function followUser(Request $request){
    	$follower = new Follower;
    	$follower->user_id = Auth::user()->id;
    	$follower->following_id = $request->user_id;
    	$follower->save();
    	return redirect()->back();
    }

        public function unfollowUser(Request $request){
    	$follower = Follower::where('user_id',Auth::user()->id)->where('following_id',$request->user_id)->delete();
    	return redirect()->back();

}	
 
}
