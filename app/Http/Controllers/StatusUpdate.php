<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Carbon\Carbon;
use App\Media;
use App\Event;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;

class StatusUpdate extends Controller
{
   

	function status_update(Request $status_post){
		$user_id = $this->user()->id;
		$post = new Post;
		$post->owner_id = $user_id;
		$post->post_type = 'STATUSUPDATE';
		$post->post_text = $status_post->status;
		$post->post_media_id = NULL;
		$post->post_status = 'POSTED';
		$post->post_privacy = 'public';
		$post->save();
		return redirect('/home');

	}
	function upload_photo(Request $photo_details){
		$user_id = $this->user()->id;
		$unique_name = $user_id.Carbon::now()->format('YmdHisu');
		if ($photo = $photo_details->file('upload_photo')){
			$file_name = $unique_name.".".$photo->getClientOriginalExtension();
			if ($photo->move('media',$file_name)){
				
				//print_r($media->id);
				
				$post = new Post;
				$post->owner_id = $user_id;
				$post->post_type = 'PHOTOUPLOAD';
				$post->post_text = $photo_details->photo_caption;
				$post->post_status = 'POSTED';
				$post->post_privacy = $photo_details->photo_privacy;
				$post->save();

				$media = new Media;
				$media->media_name = $file_name;
				$media->media_url= 'media/'.$file_name;
				$media->media_type = 'PHOTO';
				$media->media_description = $photo_details->photo_caption;
				$media->media_owner_id = $user_id;
				$media->post_id=$post->id;
				$media->save();
				return redirect('/home'); 
			}
		}
		/**/
	}

	function upload_video(Request $video_details){
		$user_id = $this->user()->id;
		$unique_name = $user_id.Carbon::now()->format('YmdHisu');
		if ($photo = $video_details->file('upload_video')){
			$file_name = $unique_name.".".$photo->getClientOriginalExtension();
			if ($photo->move('media',$file_name)){
				
				//print_r($media->id);
				
				$post = new Post;
				$post->owner_id = $user_id;
				$post->post_type = 'VIDEOUPLOAD';
				$post->post_text = $video_details->video_caption;
				$post->post_status = 'POSTED';
				$post->post_privacy = $video_details->video_privacy;

				$post->save();

				$media = new Media;
				$media->media_name = $file_name;
				$media->media_url= 'media/'.$file_name;
				$media->media_type = 'VIDEO';
				$media->media_description = $video_details->video_caption;
				$media->media_owner_id = $user_id;
				$media->post_id=$post->id;
				$media->save();
				return redirect('/home'); 
			}
		}
		/**/
	}

	function create_event(Request $event_details){
		$user_id = $this->user()->id;

			//dd($event_details);
				$post = new Post;
				$post->owner_id = $user_id;
				$post->post_type = 'EVENT';
				$post->post_text = $event_details->event_desc;
				$post->post_status = 'POSTED';
				$post->post_privacy = $event_details->event_privacy;

				$post->save();

				$event = new Event;
				$event->owner_id = $user_id;
				$event->event_time = $event_details->event_time;
				$event->event_date = $event_details->event_date;
				$event->event_venue = $event_details->event_location;
				$event->event_type = $event_details ->event_type;
				$event->event_description = $event_details->event_desc;
				$event->event_status = 'CREATED';
				$event->event_name= $event_details->event_name;
				$event->participant_type= $event_details->part_type;
				$event->post_id = $post->id;
				$event->save();
				return redirect('/home');
		}
		/**/
	
}
