<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Media;
use App\Follower;

class GetProfile extends Controller
{
    //
    public function getProfile(Request $request){
    	$userId = $request->user_id;
    	$user_details= User::where('id',$userId)->first();
    	$user_media=Media::where('media_owner_id',$userId)->get()->reverse();
    	$user_posts=Post::where('owner_id',$userId)
    				//->where
    				->get()
    				->reverse();
    	$follow_status = Follower::where('user_id',Auth::user()->id)->where('following_id',$userId)->exists();
    	//dd($user_posts);
    	//dd($user_media);
    	//dd($user_details);
    	//dd($follow_status);
	return view('profile',['user_details' => $user_details,'user_posts'=> $user_posts,'media_details' => $user_media, 'follow_status' => $follow_status]);
    }
}
