<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Post;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $following = Auth::user()->following->pluck('id');
        //dd($following);
       $posts = Post::whereIn('owner_id',$following)
                        //->orWhere('owner_id',$this->user()->id)
                        ->orWhere('post_privacy','PUBLIC')
                       // ->take(10)
                        ->get()
                        ->reverse();

        //$posts = Auth::user()->posts->reverse();
        //dd($posts);
        return view('news_feed',[
            'posts' => $posts
        ]);
    }
}
