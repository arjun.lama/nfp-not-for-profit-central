<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EditProfile extends Controller
{
    //
    public function EditProfile(){
    	$user_details = Auth::user();
    	return view('edit_profile', ['user_details' => $user_details]);
    }
}
