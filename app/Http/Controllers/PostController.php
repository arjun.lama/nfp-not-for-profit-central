<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Repo\PostRepository;

class PostController extends BaseController
{
  protected $postRepo;

  public function __construct(PostRepository $postRepo) 
  {
  		$this->postRepo = $postRepo;
  }	

  public function createPost(Request $request) 
  {
    
       $data = $request->except('_token');
       $result = $this->postRepo->create($data);
       if($result) {
       		echo 'success';
       } else {
       	echo 'fail';
       }
  }   
}