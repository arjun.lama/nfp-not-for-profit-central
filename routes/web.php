<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing_page');
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/profile', 'GetProfile@getProfile')->name('profile_link');

Route::get('/follow', 'FollowUser@followUser')->name('follow_user');

Route::get('/unfollow', 'FollowUser@unfollowUser')->name('unfollow_user');

Route::get('/edit_profile', 'EditProfile@editProfile')->name('edit_profile');

Route::post('/submit_status', 'StatusUpdate@status_update');

Route::post('/upload_photo', 'StatusUpdate@upload_photo');

Route::post('/upload_video', 'StatusUpdate@upload_video');

Route::post('/create_event', 'StatusUpdate@create_event');

Route::post('/add_comment', 'CommentController@createComment');

Route::get('/get_comments', 'CommentController@getComments');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
